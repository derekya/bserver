package com.example.marketmaker.net;

public class QuoteRequest {

    private final int symbol;
    private final boolean isBuy;
    private final int quantity;

    public QuoteRequest(int symbol, boolean isBuy, int quantity) {
        this.symbol = symbol;
        this.isBuy = isBuy;
        this.quantity = quantity;
    }

    public int getSymbol() {
        return symbol;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public int getQuantity() {
        return quantity;
    }
}
