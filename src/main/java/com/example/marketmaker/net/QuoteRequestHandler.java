package com.example.marketmaker.net;

import com.example.marketmaker.quote.QuoteCalculationEngine;
import com.example.marketmaker.quote.ReferencePriceSource;
import com.example.marketmaker.quote.ReferencePriceSourceListener;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.internal.ObjectUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * QuoteRequestHandler to handle quote request, call CalculationEngine and return price of quote back to client
 */
public class QuoteRequestHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger LOG = LoggerFactory.getLogger(QuoteRequestHandler.class);
    private static final String BUY = "BUY";
    private static final String SELL = "SELL";
    private static final String WHITE_SPACE = " ";

    private final QuoteCalculationEngine calculationEngine;
    private final ReferencePriceSource priceSource;
    private final ReferencePriceSourceListener priceSourceListener;

    public QuoteRequestHandler(QuoteCalculationEngine calculationEngine, ReferencePriceSource priceSource, ReferencePriceSourceListener priceSourceListener) {
        ObjectUtil.checkNotNull(calculationEngine, "QuoteCalculationEngine is Null");
        ObjectUtil.checkNotNull(priceSource, "ReferencePriceSource is Null");
        ObjectUtil.checkNotNull(priceSourceListener, "ReferencePriceSourceListener is Null");

        this.calculationEngine = calculationEngine;
        this.priceSource = priceSource;
        this.priceSourceListener = priceSourceListener;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        LOG.info("channel active! subscribed Price Source Listener!");
        priceSource.subscribe(priceSourceListener);
        super.channelActive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        LOG.error("Exception happens when handling quote request", cause);
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        LOG.info("Getting data from channel: " + msg);
        QuoteRequest quote = getQuoteRequest(msg);
        double sourcePrice = priceSource.get(quote.getSymbol());
        double finalPrice = calculationEngine.calculateQuotePrice(quote.getSymbol(), sourcePrice, quote.isBuy(), quote.getQuantity());
        String finalPriceStr = String.format("%.2f\r\n", finalPrice);
        ctx.writeAndFlush(finalPriceStr);
        LOG.info("Writing data back to channel: " + finalPriceStr);
    }

    //verify the incoming message and return QuoteRequest if message is valid
    private QuoteRequest getQuoteRequest(String msg) {
        String[] line = msg.split(WHITE_SPACE);
        String errorMsg = "";
        if (line.length != 3) {
            errorMsg = "Wrong data format received from client: " + msg;
        } else if (Ints.tryParse(line[0].trim()) == null || Ints.tryParse(line[2].trim()) == null) {
            errorMsg = "Symbol/Quantity isn't numeric: " + msg;
        } else if (!line[1].trim().toUpperCase().equals(BUY) && !line[1].trim().toUpperCase().equals(SELL)) {
            errorMsg = "Invalid signal for quote(not BUY or SELL): " + line[1];
        }

        if (!Strings.isNullOrEmpty(errorMsg)) {
            LOG.error(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }

        int symbol = Integer.parseInt(line[0].trim());
        boolean isBuy = line[1].trim().toUpperCase().equals(BUY);
        int quantity = Integer.parseInt(line[2].trim());

        return new QuoteRequest(symbol, isBuy, quantity);
    }
}
