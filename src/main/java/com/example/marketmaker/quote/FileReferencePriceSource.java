package com.example.marketmaker.quote;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * One implementation of ReferencePriceSource based on file
 * Read reference price from file and save it in memory
 */
public class FileReferencePriceSource implements ReferencePriceSource {

    private ReferencePriceSourceListener sourceListener;

    private final Map<Integer, BigDecimal> priceMap;

    public FileReferencePriceSource(String fileName) throws IOException, URISyntaxException {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("Empty file name for Reference Price Source");
        }
        priceMap = loadReferencePrice(fileName);
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        this.sourceListener = listener;
    }

    @Override
    public double get(int securityId) {
        BigDecimal price = priceMap.getOrDefault(securityId, BigDecimal.ZERO);
        if (sourceListener != null && !price.equals(BigDecimal.ZERO)) {
            sourceListener.referencePriceChanged(securityId, price.doubleValue());
        }
        return price.doubleValue();
    }

    public Map<Integer, BigDecimal> getPriceMap() {
        return priceMap;
    }

    private Map<Integer, BigDecimal> loadReferencePrice(String fileName) throws IOException, URISyntaxException {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new FileNotFoundException(fileName + " doesn't exist");
        }
        Path path = Paths.get(resource.toURI());
        Stream<String> lines = Files.lines(path);
        Map<Integer, BigDecimal> result = lines
                .map(s -> s.split(","))
                .collect(Collectors.toMap(array -> Integer.valueOf(array[0].trim()), array -> new BigDecimal(array[1].trim())));
        lines.close(); // explicit close stream as it involves I/O operation
        return result;
    }
}
