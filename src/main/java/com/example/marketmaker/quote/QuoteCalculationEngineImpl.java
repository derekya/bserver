package com.example.marketmaker.quote;

/**
 * One simple implementation of QuoteCalculationEngine that returns quote price by security price times quantity
 * If it's buy order return positive number otherwise negative number
 */
public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        return referencePrice * quantity * ((buy) ? 1 : -1);
    }

}
