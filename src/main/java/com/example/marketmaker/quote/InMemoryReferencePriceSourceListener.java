package com.example.marketmaker.quote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * One implementation of ReferencePriceSourceListener. It's called to update the memory when a price is changed
 */
public class InMemoryReferencePriceSourceListener implements ReferencePriceSourceListener {

    private final static Logger LOG = LoggerFactory.getLogger(InMemoryReferencePriceSourceListener.class);
    //estimated capacity for symbols/prices it supports
    private Map<Integer, Double> latestReferencePrice = new ConcurrentHashMap<>(2048);

    public Map<Integer, Double> getLatestReferencePrice() {
        return latestReferencePrice;
    }

    //TODO here's just purely update the latest price in memory but it could support more actions if required. For example, call downstream with latest price
    @Override
    public void referencePriceChanged(int securityId, double price) {
        LOG.debug("Updating " + securityId + " with lastest price: " + price);
        latestReferencePrice.put(securityId, price);
    }
}
