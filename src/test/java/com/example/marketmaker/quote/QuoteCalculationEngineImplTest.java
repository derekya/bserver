package com.example.marketmaker.quote;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class QuoteCalculationEngineImplTest {

    private QuoteCalculationEngineImpl underTest;
    private int symbol = 700;

    @Before
    public void setup() {
        underTest = new QuoteCalculationEngineImpl();
    }

    @Test
    public void givenBuyQuote_thenReturnPositiveQuotePrice() {
        double result = underTest.calculateQuotePrice(symbol, 333.24, true, 100);
        Assert.assertEquals(33324, result, 0.000001);
    }

    @Test
    public void givenSellQuote_thenReturnNegativeQuotePrice() {
        double result = underTest.calculateQuotePrice(symbol, 333.24, false, 100);
        Assert.assertEquals(-33324, result, 0.000001);
    }

    @Test
    public void givenQuote_whenNoReferencePrice_thenReturnZero() {
        double result = underTest.calculateQuotePrice(symbol, 0, true, 100);
        Assert.assertEquals(0, result, 0.000001);
    }

}
