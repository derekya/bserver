package com.example.marketmaker.quote;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;

@RunWith(JUnit4.class)
public class InMemoryReferencePriceSourceTest {

    private InMemoryReferencePriceSourceListener underTest;
    private int testSymbol = 700;
    private double testPrice = 333.24;

    @Before
    public void setup() {
        underTest = new InMemoryReferencePriceSourceListener();
    }

    @Test
    public void givenSymbolAndPrice_whenUpdateOnce_andGetPrice() {

        underTest.referencePriceChanged(testSymbol, testPrice);

        Map<Integer, Double> testMap = underTest.getLatestReferencePrice();
        Assert.assertTrue(testMap.containsKey(testSymbol));
        Assert.assertEquals(testPrice, testMap.get(testSymbol), 0.000001);
    }

    @Test
    public void givenSymbolAndPrice_whenUpdateTwice_andGetLastestPrice() {
        underTest.referencePriceChanged(testSymbol, testPrice);
        double latestPrice = 444.44;
        underTest.referencePriceChanged(testSymbol, latestPrice);

        Map<Integer, Double> testMap = underTest.getLatestReferencePrice();
        Assert.assertTrue(testMap.containsKey(testSymbol));
        Assert.assertEquals(latestPrice, testMap.get(testSymbol), 0.000001);

    }
}
