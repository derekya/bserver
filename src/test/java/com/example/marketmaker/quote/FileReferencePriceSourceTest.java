package com.example.marketmaker.quote;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class FileReferencePriceSourceTest {

    private FileReferencePriceSource underTest;
    private double delta = 0.000001;

    @Mock
    private ReferencePriceSourceListener listener;

    @Test(expected = IllegalArgumentException.class)
    public void givenNullOrEmptyFilename_thenThrowException() throws IOException, URISyntaxException {
        underTest = new FileReferencePriceSource("");
    }

    @Test(expected = FileNotFoundException.class)
    public void givenNotExistingFile_thenThrowException() throws IOException, URISyntaxException {
        underTest = new FileReferencePriceSource("non-exist-file-name");
    }

    @Test
    public void givenExistingFile_thenCheckContentOfMap() throws IOException, URISyntaxException {
        underTest = new FileReferencePriceSource("ReferencePrice-test.csv");
        Map<Integer, BigDecimal> priceMap = underTest.getPriceMap();

        Assert.assertFalse(priceMap.isEmpty());
        Assert.assertTrue(priceMap.containsKey(1));
        Assert.assertTrue(priceMap.containsKey(5));
        Assert.assertTrue(priceMap.containsKey(700));
        Assert.assertTrue(priceMap.containsKey(388));
        Assert.assertTrue(priceMap.containsKey(1299));

        Assert.assertEquals(71.51, priceMap.get(1).doubleValue(), delta);
        Assert.assertEquals(58.15, priceMap.get(5).doubleValue(), delta);
        Assert.assertEquals(333.60, priceMap.get(700).doubleValue(), delta);
        Assert.assertEquals(245.80, priceMap.get(388).doubleValue(), delta);
        Assert.assertEquals(77.30, priceMap.get(1299).doubleValue(), delta);

        Assert.assertFalse(priceMap.containsKey(2));
    }

    @Test
    public void givenExistingFile_thenCheckPrice() throws IOException, URISyntaxException {
        underTest = new FileReferencePriceSource("ReferencePrice-test.csv");

        underTest.subscribe(listener);

        Assert.assertEquals(71.51, underTest.get(1), delta);
        Assert.assertEquals(58.15, underTest.get(5), delta);
        Assert.assertEquals(333.60, underTest.get(700), delta);
        Assert.assertEquals(245.80, underTest.get(388), delta);
        Assert.assertEquals(77.30, underTest.get(1299), delta);

        Mockito.verify(listener, Mockito.times(1)).referencePriceChanged(1, 71.51);
        Mockito.verify(listener, Mockito.times(1)).referencePriceChanged(5, 58.15);
        Mockito.verify(listener, Mockito.times(1)).referencePriceChanged(700, 333.60);
        Mockito.verify(listener, Mockito.times(1)).referencePriceChanged(388, 245.80);
        Mockito.verify(listener, Mockito.times(1)).referencePriceChanged(1299, 77.30);

        Assert.assertEquals(BigDecimal.ZERO.doubleValue(), underTest.get(2), delta);
        Mockito.verify(listener, Mockito.times(0)).referencePriceChanged(2, BigDecimal.ZERO.doubleValue());
    }

}
