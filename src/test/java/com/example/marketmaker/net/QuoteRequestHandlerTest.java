package com.example.marketmaker.net;


import com.example.marketmaker.quote.QuoteCalculationEngine;
import com.example.marketmaker.quote.QuoteCalculationEngineImpl;
import com.example.marketmaker.quote.ReferencePriceSource;
import com.example.marketmaker.quote.ReferencePriceSourceListener;
import io.netty.channel.ChannelHandlerContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class QuoteRequestHandlerTest {

    private QuoteRequestHandler underTest;

    @Mock
    private QuoteCalculationEngine engine;
    @Mock
    private ReferencePriceSource priceSource;
    @Mock
    private ReferencePriceSourceListener listener;
    @Mock
    private ChannelHandlerContext ctx = Mockito.mock(ChannelHandlerContext.class);


    @Before
    public void setup() {
        engine = new QuoteCalculationEngineImpl();
        priceSource = Mockito.mock(ReferencePriceSource.class);
        listener = Mockito.mock(ReferencePriceSourceListener.class);
        ctx = Mockito.mock(ChannelHandlerContext.class);

        underTest = new QuoteRequestHandler(engine, priceSource, listener);
    }

    @Test(expected = NullPointerException.class)
    public void givenNull_thenThrowException() {
        underTest = new QuoteRequestHandler(null, priceSource, listener);
    }

    @Test
    public void givenActiveChannel_thenSubscribe() throws Exception {
        underTest.channelActive(ctx);
        Mockito.verify(priceSource, Mockito.times(1)).subscribe(listener);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenIncompleteMessage_whenRead_thenThrowException(){
        String invalidMessage = "123 BUY\r\n";
        underTest.channelRead0(ctx, invalidMessage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenNotNumericQuantityMessage_whenRead_thenThrowException(){
        String invalidMessage = "123 BUY xxx\r\n";
        underTest.channelRead0(ctx, invalidMessage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenNotNumericSymbolMessage_whenRead_thenThrowException(){
        String invalidMessage = "xxx BUY 123\r\n";
        underTest.channelRead0(ctx, invalidMessage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenNotBuySellMessage_whenRead_thenThrowException(){
        String invalidMessage = "123 test 123\r\n";
        underTest.channelRead0(ctx, invalidMessage);
    }

    @Test
    public void givenBuyValidMessage_whenRead_thenReturnQuotePrice() {
        String validMessage = "123 BUY 100\r\n";
        Mockito.when(priceSource.get(123)).thenReturn(200.00);
        underTest.channelRead0(ctx, validMessage);
        Mockito.verify(ctx, Mockito.times(1)).writeAndFlush("20000.00\r\n");

        validMessage = "700 BUY 200\r\n";
        Mockito.when(priceSource.get(700)).thenReturn(333.22);
        underTest.channelRead0(ctx, validMessage);
        Mockito.verify(ctx, Mockito.times(1)).writeAndFlush("66644.00\r\n");

        validMessage = "333 BUY 100\r\n";
        underTest.channelRead0(ctx, validMessage);
        Mockito.verify(ctx, Mockito.times(1)).writeAndFlush("0.00\r\n");
    }

    @Test
    public void givenSellValidMessage_whenRead_thenReturnQuotePrice(){
        String validMessage = "123 SELL 200\r\n";
        Mockito.when(priceSource.get(123)).thenReturn(200.00);
        underTest.channelRead0(ctx, validMessage);
        Mockito.verify(ctx, Mockito.times(1)).writeAndFlush("-40000.00\r\n");

        validMessage = "700 SELL 200\r\n";
        Mockito.when(priceSource.get(700)).thenReturn(333.22);
        underTest.channelRead0(ctx, validMessage);
        Mockito.verify(ctx, Mockito.times(1)).writeAndFlush("-66644.00\r\n");

        validMessage = "333 SELL 100\r\n";
        underTest.channelRead0(ctx, validMessage);
        Mockito.verify(ctx, Mockito.times(1)).writeAndFlush("-0.00\r\n");
    }

}
