package com.example.marketmaker.client;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestClientHandler extends SimpleChannelInboundHandler<String> {
    private static final Logger LOG = LoggerFactory.getLogger(TestClientHandler.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        LOG.info("Activating and sending...");
        String str = "700 BUY 100\r\n";
        ChannelFuture future = ctx.writeAndFlush(str);
        future.addListener((ChannelFutureListener) future1 -> LOG.info("Sent complete!"));
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        System.err.println(msg);
        LOG.info("received from server: " + msg);
        ctx.close();
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
